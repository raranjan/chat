from rasa_nlu.training_data import load_data
from rasa_nlu.model import Trainer
from rasa_nlu import config
from rasa_nlu.model import Interpreter
import logging

data_path = './data/nlu-data.md'
config_path = 'nlu_config.yml'
model_path = './model/nlu'
model_name = model_path + '/default/bot'

def train_bot(data, config_file, model_dir):
    training_data = load_data(data)
    trainer = Trainer(config.load(config_file))
    trainer.train(training_data)
    model_directory = trainer.persist(model_dir, fixed_model_name='bot')
    logging.info('Saved model in location: {}'.format(model_directory))

def predict(text):
    interpreter = Interpreter.load(model_name)
    print(interpreter.parse(text))


if __name__ == "__main__":
    # train_bot(data=data_path, config_file=config_path, model_dir=model_path)
    predict("get trade data for today")