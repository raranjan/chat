import sqlite3
from random import randint
import logging
import sys

database = '/Users/rakesh/Workspace/Python/Chat/sqlite.db'

def create_table(conn):
    print('Creating Trade table')
    CREATE_TRADE_TABLE_QUERY = '''CREATE TABLE IF NOT EXISTS TRADE (
        id integer PRIMARY KEY,
        quantity integer NOT NULL,
        date text,
        rate integer,
        currency text,
        fee integer,
        creation_date text,
        created_by text)'''
        
    curr = conn.cursor()
    curr.execute(CREATE_TRADE_TABLE_QUERY)


def create_trade(conn, trade):
    INSERT_QUERY = '''INSERT INTO TRADE(ID, QUANTITY, DATE, RATE, CURRENCY, FEE, CREATION_DATE, CREATED_BY) 
    VALUES(?,?,?,?,?,?,?,?)'''

    curr = conn.cursor()
    curr.execute(INSERT_QUERY, trade)
    return curr.lastrowid


def populate_trade_data(conn):
    for i in range (50):
        trade_id = i
        trade_quantity = randint(10, 99)
        trade_date = create_date('2019-04-{}'.format(i%5 if i%5!= 0 else 1))
        trade_rate = randint(10, 99)
        trade_currency = 'USD'
        trade_fee = trade_rate * trade_quantity
        trade_creation_date = create_date()
        trade_created_by = 'rr45447'

        trade = (trade_id, trade_quantity, trade_date, trade_rate, trade_currency, trade_fee, trade_creation_date, trade_created_by)

        curr_id = create_trade(conn, trade)
        logging.info('Created trade data with id: {}'.format(curr_id))


def create_connection(db_file):
    try:
        conn = sqlite3.connect(db_file)
        return conn
    except ConnectionError as e:
        print(e)


def init_db():
    conn = create_connection(database)
    create_table(conn)
    with conn:
        populate_trade_data(conn)


def create_date(datestr="", format="%Y-%m-%d"):
    from datetime import datetime
    if not datestr:
        return datetime.today().date()
    return datetime.strptime(datestr, format).date()


def get_trade_for_id(id):
    GET_TRADE_QUERY = 'SELECT * from TRADE where id = ?'
    row = execute_query(GET_TRADE_QUERY, (id,))
        
    return row


def get_trade_for_date(date):
    GET_TRADE_QUERY = 'SELECT * from TRADE where date = ?'
    row = execute_query(GET_TRADE_QUERY, (date,))

    return row


def execute_query(query, params):
    conn = create_connection(database)

    with conn:
        cur = conn.cursor()
        cur.execute(query, params)
        resultset = cur.fetchall()

        return resultset


def main():
#     init_db()
    print(get_trade_for_id(1))
    print(get_trade_for_date('2019-04-01'))

if __name__ == '__main__':
    main()