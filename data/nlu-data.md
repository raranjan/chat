## intent:goodbye
- Bye
- Goodbye
- See you later
- Bye bot
- Goodbye friend

## intent:greet
- Hi
- Hey
- Hi bot
- Hey bot
- Hello

## intent:thanks
- Thanks
- Thank you
- Thank you so much

## intent:affirm
- y
- Y
- yes
- yes sure
- absolutely
- sure
- hmmm

## intent:deny
- no
- never
- I don't think so

## intent:current_matches
- which cricket match is happening right now
- which ipl match is next
- which teams are playing next in ipl
- tell me some ipl news
- i want ipl updates
- what are the latest match updates
- who won the last ipl match
- how is ipl going
- what was the result of the last match
- when is the next match

## intent:current_trade
- tell me latest trade details
- what is trade details for today
- trade today
- trade data
- trade details
- today's trade
- today's trade data
- wondering how is trade doing today
- how was trade today

## intent:trade_for_date
- trade data for [1](DD)-[jan](MMM)-[2019](YYYY)
- trade for [1st](DDD) [jan](MMM)
- could you show me trade details for date [2019](YYYY)-[02](MM)-[24](DD)
- wondering what is trade for [1](DD)-[mar](MMM)-[19](YY)
- how was trade for [14](DD)-[may](MMM)-[2018](YYYY)
- trade on [2nd](DDD) [jan](MMM) [19](YY)
- trade in [3rd](DDD) [august](MMMM) [2018](YYYY)
- share details for trade happened in [4th](DDD) [january](MMMM) [2019](YYYY)
- show me trade for [13th](DDD) [JAN](MMM)
- trade for [1st](DDD) (july)[MMMM]
- [2](DD) [jan](DDD) [2019](YYYY)
- [12](DD) [jan](MMM) [18](YY)
- trade for [01](DD)-[02](MM)-[18](YY)
- trade: [01](DD)/[02](MM)/[2019](YYYY)
- trade details for


## intent:trade_for_id
- trade for [1234](id)
- trade: [3452](id)
- show me info for trade with id [98765](id)
- trade id [87654](id)