## trade path 1
* greet
 - utter_greet
* current_trade
 - action_current_trade
 - utter_did_that_help
* affirm or thanks
 - utter_gratitude
* goodbye
 - utter_goodbye